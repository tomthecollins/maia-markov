"use strict";

require('./Graph');

var dataEx = [{
  "city": "farnham",
  "nbs": [{
    "city": "aldershot",
    "dist": 3
  }, {
    "city": "guildford",
    "dist": 7
  }]
}, {
  "city": "aldershot",
  "nbs": [{
    "city": "farnham",
    "dist": 3
  }]
}, {
  "city": "guildford",
  "nbs": [{
    "city": "aldershot",
    "dist": 3
  }, {
    "city": "farnham",
    "dist": 9
  }]
}];
var g = new Graph(dataEx, "city", "nbs", "dist");
console.log("g:", g);